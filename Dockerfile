FROM node:lts as base

WORKDIR /home/node/app

COPY package*.json ./

RUN npm install

COPY src ./
COPY tsconfig.json ./
COPY tsconfig.json ./