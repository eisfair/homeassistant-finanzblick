import {Page} from "puppeteer";
import {sleep, toNumber} from "./helper";
import * as speakeasy from "speakeasy";
import {MqttClient} from "mqtt";

interface FinanzBlickProps {
    mqttClient: MqttClient,
    totpSecret: string,
    budgetNameMap: Record<string,string>
    loginUsername: string,
    loginPassword: string,
}

export class FinanzBlick {

    private readonly mqttClient;
    private readonly totpSecret;
    private readonly budgetNameMap;
    private readonly username;
    private readonly password;

    private readonly unitOfMeasurement = '€';
    private readonly deviceClass = 'monetary';
    private readonly baseTopic = 'homeassistant/number/';

    constructor(props: FinanzBlickProps) {
        this.mqttClient = props.mqttClient
        this.totpSecret = props.totpSecret;
        this.budgetNameMap = props.budgetNameMap;
        this.username = props.loginUsername;
        this.password = props.loginPassword;
    }
    async updateBanking(page: Page) {
        await sleep(5000);

        const refreshButton = await page.waitForSelector('.icon-ic_aktualisieren_alle_24')

        await refreshButton!.click()

        await sleep(60 * 1000);
    }

    async getAccountBalances(page: Page) {
        const cardListItems = await page.$$('.cardList > div.card:not(.cdk-drag-disabled)');

        const accounts = new Map();

        for(const accountItem of cardListItems) {

            const accountNameText = await accountItem.waitForSelector('.cardListItem__name')
            const accountIBANText = await accountItem.waitForSelector('.cardListItem__iban')
            const accountBalanceText = await accountItem.waitForSelector('.cardListItem__amount');
            const accountLastUpdateText = await accountItem.waitForSelector('.cardListItem__lastRefreshText');

            const name = (await accountNameText!.evaluate(el => el.textContent))!.trim();
            const iban = await accountIBANText!.evaluate(el => el.textContent);
            const balanceText = await accountBalanceText!.evaluate(el => el.textContent);
            const lastUpdate = await accountLastUpdateText!.evaluate(el => el.textContent);

            let balance = toNumber(balanceText!);

            if(balanceText!.trim().substring(0, 1) === '-') {
                balance *= -1;
            }

            accounts.set(name, {
                name: name,
                iban: iban!.trim(),
                balance: balance + '',
                lastUpdate: lastUpdate!.trim()
            });
        }

        return accounts;
    }

    async getTotalBalance(page: Page) {
        const balanceElement = await page.waitForSelector('.headerContainer__title > div:nth-child(2)');
        const balanceText = await balanceElement!.evaluate(el => el.textContent) ?? '';
        let balance = toNumber(balanceText);

        if(balanceText.trim().substring(0, 1) === '-') {
            balance *= -1;
        }

        return balance;
    }

    async getBudgets(page: Page) {
        const budgetListButton = await page.waitForSelector('.sidebarMenu .menu-ic_menu_budgets_off_24');
        await budgetListButton!.click();

        await sleep(10000)

        const budgetEntries = await page.$$('app-budget-card-list-item > div')
        const budgets = [];

        console.log('budget entries', budgetEntries);

        for(const listItem of budgetEntries) {

            await listItem.click();

            await sleep(1000)

            const budgetNameText = await listItem.waitForSelector('.cardListItem__name')
            const limitText = await page.waitForSelector('.budget__leftText');
            const budgetText = await page.waitForSelector('.budgetGraphic__usedText');

            const name = await budgetNameText!.evaluate(el => el.textContent);
            const limit = await limitText!.evaluate(el => el.textContent);
            const current = await budgetText!.evaluate(el => el.textContent);

            budgets.push({
                name: name!.trim(),
                limit: toNumber(limit!),
                current: toNumber(current!)
            })
        }

        console.log('budgets', budgets);

        return budgets;
    }

    async login(page: Page) {

        const totp = speakeasy.totp({ secret: this.totpSecret,
            algorithm: 'sha1',
            encoding: 'base32'
        });

        // Query for an element handle.
        const loginEmail = await page.waitForSelector('#eml-user-login');
        const loginPassword = await page.waitForSelector('#psw-user-login');
        const loginSubmit = await page.waitForSelector('#form-login-submit');

        await loginEmail!.type(this.username)
        await loginPassword!.type(this.password)
        await loginSubmit!.click()

        await sleep(5000)

        const totpInput = await page.waitForSelector('#twoFactorActivate');

        await totpInput!.type(totp)

        const totpSubmit = await page.waitForSelector('#ms-two-factor-submit');

        await totpSubmit!.click()

        await page.waitForNavigation();

        console.log('navigation finished')

        await sleep(10000)
    }

    private mapBudgetName(budgetName: string) {
        for( const key in this.budgetNameMap) {
            if(this.budgetNameMap[key] === budgetName) {
                console.log('return', key, 'for', budgetName);
                return key;
            }
        }

        throw new Error('Budget Name not found: ' + budgetName);
    }

    private generateHADiscoveryConfig(props: {
        identifier: string;
        deviceName: string;
        min: number;
        max: number;
    }) {

        const uniqueId = "finanzblick_" + props.identifier;

        return {
            "name": null,
            "device_class":this.deviceClass,
            "unique_id": uniqueId,
            "entity_id": 'number.' + uniqueId,
            "config_topic": `${this.baseTopic}${uniqueId}/config`,
            "state_topic": `${this.baseTopic}${uniqueId}/state`,
            "command_topic": `${this.baseTopic}${uniqueId}/command`,
            "unit_of_measurement": this.unitOfMeasurement,
            "mode": "slider",
            "device":{
                "name": props.deviceName,
                "identifiers": [
                    uniqueId
                ]
            },
            "min": props.min,
            "max": props.max,
        }
    }

    sendBudgetConfig(name: string, limit: number, current: number) {

        const shortName = this.mapBudgetName(name);
        const budgetName = 'budget_' + shortName;

        const payload = this.generateHADiscoveryConfig({
            deviceName: shortName,
            identifier: budgetName,
            min: limit - current > 0 ? 0 : limit - current,
            max: limit
        })

        const state_value = limit - current;

        this.mqttClient.publish(payload.config_topic, JSON.stringify(payload));
        this.mqttClient.publish(payload.state_topic, state_value+'');

        console.log('sending budget', payload, state_value);
    }

    sendBalanceConfig(balance: number) {
        const payload = this.generateHADiscoveryConfig({
            deviceName: 'Kontostand',
            identifier: 'balance',
            min: -10000,
            max: 100000,
        })

        this.mqttClient.publish(payload.config_topic, JSON.stringify(payload));
        this.mqttClient.publish(payload.state_topic, balance+'')
    }

    sendAccountBalanceConfig(attributes: Record<string, string>) {

        const identifier = 'account_' + attributes.name.replace(' ', '_');

        const payload = this.generateHADiscoveryConfig({
            deviceName: `Konto ${identifier}`,
            identifier: identifier,
            min: -10000,
            max: 100000,
        })

        this.mqttClient.publish(payload.config_topic, JSON.stringify(payload));
        this.mqttClient.publish(payload.state_topic, attributes.balance)

        console.log('sending account', payload, attributes);
    }
}