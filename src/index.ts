import puppeteer, {PuppeteerLaunchOptions} from 'puppeteer';
// @ts-ignore
import {FinanzBlick} from "./FinanzBlick";
import * as mqtt from "mqtt";
import * as process from "process";

const appConfig = JSON.parse(process.env.APP_CONFIG!) as AppConfig

export interface AppConfig {
    totpSecret: string,
    haMqttUser : string,
    haMqttPassword: string,
    haMqttHost: string,
    haMqttClientId: string,
    haMqttPort: number,
    haMqttProtocol: string,
    puppeteerOptions: PuppeteerLaunchOptions,
    finanzBlickBudgetNameMapping: Record<string, string>
    finanzBlickUsername: string,
    finanzBlickPassword: string,
    finanzBlickLoginUrl: string,
    defaultPageTimeout: number,
}

const connectUrl = `${appConfig.haMqttProtocol}://${appConfig.haMqttHost}:${appConfig.haMqttPort}`
const mqttClient = mqtt.connect(connectUrl, {
    clientId: appConfig.haMqttClientId,
    clean: true,
    connectTimeout: 10000,
    reconnectPeriod: 10000,
    username: appConfig.haMqttUser,
    password: appConfig.haMqttPassword,
})

async function start() {

    const browser = await puppeteer.launch(appConfig.puppeteerOptions);

    try {
        const finanzBlick = new FinanzBlick({
            mqttClient,
            totpSecret: appConfig.totpSecret,
            budgetNameMap: appConfig.finanzBlickBudgetNameMapping,
            loginPassword: appConfig.finanzBlickPassword,
            loginUsername: appConfig.finanzBlickUsername,
        })

        const page = await browser.newPage();

        page.setDefaultNavigationTimeout(appConfig.defaultPageTimeout)
        page.setDefaultTimeout(appConfig.defaultPageTimeout)

        await page.setViewport({ width: 1920, height: 768});

        await page.goto(appConfig.finanzBlickLoginUrl);

        await finanzBlick.login(page);

        await finanzBlick.updateBanking(page);

        const totalBalance = await finanzBlick.getTotalBalance(page);
        finanzBlick.sendBalanceConfig(totalBalance);

        const accountBalances = await finanzBlick.getAccountBalances(page);

        for(const account of accountBalances.values()) {
            finanzBlick.sendAccountBalanceConfig(account);
        }

        const budgets = await finanzBlick.getBudgets(page);

        budgets.map((budget) => {
            finanzBlick.sendBudgetConfig(budget.name, budget.limit, budget.current);
        });
    } catch (error) {
        console.log(error);
    } finally {
        await browser.close();

        mqttClient.end();
    }
}

mqttClient.on('connect', async () => {
    mqttClient.on('message', (topic, payload) => {
        console.log('Received Message:', topic, payload.toString())
    })

    await start();
})

mqttClient.on("error", (error) => {
    console.log(error);
})