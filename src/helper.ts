
export function timeout(ms: number) {
    return new Promise(resolve => setTimeout(resolve, ms));
}
export async function sleep(ms:number) {
    await timeout(ms);
}

export function toNumber(value: string) {
    return Math.floor(Number.parseFloat(value.replace('.', '').replace(',', '.').match((/(\d+\.\d+)/))?.shift()!))
}
